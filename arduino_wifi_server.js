var _local_ip = '192.168.50.178';
var _api = {
    'get_all_info': 'get_all_info',
    'get_analog_info': 'get_analog_info',
    'set_pin_state': 'set_pin_state',
}
var _timerId = 0;
var _update_timeout = 0;
function build_url(method) {
    return `http://${_local_ip}/${_api[method]}`
}
function send_get(url, cFunction) {
    var xhr = new XMLHttpRequest();
    xhr.onload = () => {
        if (xhr.status !== 200) {
            console.log(`Request ${url} execution error: ${xhr.status}: ${xhr.statusText}`);
            return;
        }
        const response = xhr.response;
        console.log(`Response received on request ${url}: ${response}`);
        cFunction(response);
    };
    xhr.onerror = () => {
        console.log(`Request ${url} is not executed: ${xhr.status}: ${xhr.statusText}`);
    };
    xhr.open('GET', url, true);
    xhr.send();
}
function send_post(url, data, cFunction) {
    var xhr = new XMLHttpRequest();
    xhr.onload = () => {
        if (xhr.status !== 200) {
            console.log(`Request ${url} execution error: ${xhr.status}: ${xhr.statusText}`);
            return;
        }
        const response = xhr.response;
        console.log(`Response received on request ${url}: ${response}`);
        cFunction(response);
    };
    xhr.onerror = () => {
        console.log(`Request ${url} is not executed: ${xhr.status}: ${xhr.statusText}`);
    };
    xhr.open('POST', url, true);
    xhr.send(JSON.stringify(data));
}
function isNumber(str) {
    if (typeof str != 'string') return false;
    return !isNaN(str) && !isNaN(parseFloat(str));
}
function duty_validate(obj) {
    if ((obj.value.length > 3)||(obj.value.length < 0)||(obj.value < 0)||(obj.value > 100)||(!isNumber(obj.value))) {
        document.getElementById(obj.id).style = 'border-color: #d94f5c;'
    } else {
        document.getElementById(obj.id).style = 'border-color: #474755;'
    }
}




function remove_lock(response) {
<!--    document.querySelector('.Test').textContent = response;-->
}
function parse_analog_info(response) {
    var json = JSON.parse(response);
    pins = json.a_pins.voltage;
    _update_timeout = json.a_pins.update_timeout;
    for (let i = 0; i < 6; i++) {
        document.getElementById(`a_${pins[i].pin_name}_volt`).textContent = `${pins[i].pin_voltage} V`;
    }
}
function parse_all_info(response) {
//    d_pins = response.d_pins;
//    for (let i = 0; i < 12; i++) {
//        console.log(d_pins[i].duty);
//        console.log(d_pins[i].freq);
//        console.log(d_pins[i].enabled);
//        document.getElementById(`d_${d_pins[i].pin_name}_duty`).value = d_pins[i].duty;
//        document.getElementById(`d_${d_pins[i].pin_name}_freq`).value = d_pins[i].freq;
//        document.getElementById(`d_${d_pins[i].pin_name}_enable`).value = d_pins[i].enabled;
//    }
    parse_analog_info(response);
    if (_update_timeout != 0) {
        _timerId = setInterval(get_analog_pins_info, _update_timeout);
    }
}
function get_all_info() {
    if (_update_timeout != 0 && _timerId != 0) {
        clearInterval(_timerId);
    }
    var url = build_url('get_all_info');
    send_get(url, parse_all_info);
}
function get_analog_pins_info() {
    var url = build_url('get_analog_info');
    send_get(url, parse_analog_info);
}
function set_pin_state(pin_name) {
    var url = build_url('set_pin_state');
    var current_pin_type = document.getElementById(`d_${pin_name}_name`).value;
    var pin_type;
    if (current_pin_type == none) {
        pin_type = 'digit';
    } else {
        pin_type = current_pin_type;
    }
    var data = {
        pin_name: pin_name,
        pin_type: pin_type,
        duty: document.getElementById(`d_${pin_name}_duty`).value,
        freq: document.getElementById(`d_${pin_name}_freq`).value,
        enabled: document.getElementById(`d_${pin_name}_enable`).value
    }
    send_post(url, data, remove_lock);
}
document.addEventListener('DOMContentLoaded', function(){
    get_all_info();
});